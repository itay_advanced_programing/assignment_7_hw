﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace CardWar
{
    public partial class Form1 : Form
    {
        private NetworkStream clientStream;
        private string myCard;
        private string opponentCard;
        private int myScore;
        private int opponentScore;
        private string lastCardPlayed;

        public Form1()
        {
            myCard = "0000";
            opponentCard = "0000";
            myScore = 0;
            opponentScore = 0;
            InitializeComponent();
            

            Thread myThread = new Thread(listening);
            myThread.Start();
        }
        
        /*
         * The function will generate the random card from the deck
         * Input: none
         * Output: the random card
         */
        private dynamic m_DEMO_Return_var_method()
        {
            Random rand = new Random();
            int index;
            string path = System.IO.Directory.GetCurrentDirectory();
            /*
             * Getting the path of our folder
             */
            int lastSlash = path.LastIndexOf('\\');
            path = path.Substring(0, lastSlash);
            lastSlash = path.LastIndexOf('\\');
            path = path.Substring(0, lastSlash);
            path = path + "\\Resources";
            var files = System.IO.Directory.GetFiles(path, "*.png");

            for(int i = 0; i < files.Length; i++)
            {
                int lastSlashIndex = files[i].LastIndexOf('\\');
                
                files[i] = files[i].Substring(lastSlashIndex + 1);
                int pngIndex = files[i].IndexOf(".png");
                files[i] = files[i].Substring(0, pngIndex);

                if(Char.IsNumber(files[i][0])) //Checking if the card is a number
                {
                    files[i] = "_" + files[i];
                }
            }

            index = rand.Next(files.Length);

            while((files[index].Equals(Properties.Resources.card_back_blue)) || (files[index].Equals(Properties.Resources.card_back_red))) //Making sure we don't return the empty red or blue cards
            {
                index = rand.Next(files.Length);
            }

            return files[index];
        }

        /*
         * Function will generate the 10 cards
         * Input: none
         * Output: none
         */
        private void GenerateCards()
        {
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(50, 500);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::CardWar.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    pictureBox1.Image = global::CardWar.Properties.Resources.card_back_blue; //Setting the opponent's card to be empty

                    foreach(Control j in this.Controls) //Checking if there is a card that we click on already
                    {
                        if(j.Name.Equals(lastCardPlayed)) //If there is one, we return it to be red
                        {
                            ((PictureBox)j).Image = global::CardWar.Properties.Resources.card_back_red;
                        }
                    }

                    lastCardPlayed = currentPic.Name; //Saving the card that we clicked on

                    string card = m_DEMO_Return_var_method();
                    ((PictureBox)sender1).Image = (Image)Properties.Resources.ResourceManager.GetObject(card);
                    SendToServer(card);
                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
            }
        }

        /*
         * Function will create the message and will send it to the server
         * Input: the card that we clicked on
         * Output: none
         */
        private void SendToServer(string card)
        {
            string message = "1"; //Every card message begins with 1
            int lastUnderlineIndex = card.LastIndexOf('_');
            string temp;

            if(card[0] == '_') //Checking if the card is a number
            {
                if(card[2] == '0') //Checking if the card is 10
                {
                    message = message + card[1] + card[2];
                }
                else
                {
                    message = message + "0" + card[1];
                }
            }
            else
            {
                if(card.IndexOf("ace") != -1) //Checking if the card is ace
                {
                    message = message + "01";
                }
                else if(card.IndexOf("jack") != -1) //Checking if the card is jack
                {
                    message = message + "11";
                }
                else if(card.IndexOf("queen") != -1) //Checking if the card is queen
                {
                    message = message + "12";
                }
                else //The card is king
                {
                    message = message + "13";
                }
            }

            temp = card.Substring(lastUnderlineIndex + 1); //Getting the type of the card

            temp = temp.ToUpper();

            message = message + temp[0];

            myCard = message.Substring(1);

            byte[] buffer = new ASCIIEncoding().GetBytes(message); //Sending the message to the server
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();

            if (opponentCard.Equals("0000")) //Checking if we can access the game
            {
                Thread playGame = new Thread(play);
                playGame.Start();
            }
        }

        /*
         * Function will freeze all the buttons
         * Input: none
         * Output: none
         */
        private void Freeze()
        {
            foreach(Control i in this.Controls)
            {
                i.Enabled = false;
            }
        }

        /*
         * Function will unfreeze all the buttons
         * Input: none
         * Output: none
         */
        private void Unfreeze()
        {
            foreach (Control i in this.Controls)
            {
                i.Enabled = true;
            }
        }

        /*
         * Function will listen to any message that will come from the server
         * Input: none
         * Output: none
         */
        private void listening()
        {
            Invoke((MethodInvoker)delegate { GenerateCards(); });
            Thread.Sleep(500);
            Invoke((MethodInvoker)delegate { Freeze(); });

            TcpClient client = new TcpClient();

            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

            client.Connect(serverEndPoint);

            clientStream = client.GetStream();

            bool exit = false;
            while(!exit)
            {
                byte[] buffer = new byte[4];
                int bytesRead = clientStream.Read(buffer, 0, 4);

                string input = new ASCIIEncoding().GetString(buffer);

                if (input.Equals("0000")) //Message which says that our opponent has connected to the server
                {
                    Invoke((MethodInvoker)delegate { Unfreeze(); });
                }
                else if(input[0] == '2') //Message which says that our opponent has disconected from the server
                {
                    exit = true;
                    MessageBox.Show("Your Score: " + YourScore.Text + "\nOpponent Score: " + OpponentScore.Text);
                    Application.Exit();
                }
                else //Our opponent has picked a card
                {
                    opponentCard = input.Substring(1);

                    if(myCard.Equals("0000"))
                    {
                        Thread playGame = new Thread(play);
                        playGame.Start();
                    }
                }
            }
        }

        /*
         * Function will simulates the game
         * Input: none
         * Output: none
         */
        private void play()
        {
            string opponentPic;

            while((opponentCard.CompareTo("0000") == 0) || (myCard.CompareTo("0000") == 0)) //We can play only if both we and our opponent chose a card
            {
                Thread.Sleep(500);
            }

            if(opponentCard[0] == '0') //A number between 1 to 9
            {
                if(opponentCard[1] == '1') //It is ace
                {
                    opponentPic = "ace_of_";
                }
                else //number between 2 to 9
                {
                    opponentPic = "_" + opponentCard[1] + "_of_";
                }
            }
            else
            {
                if(opponentCard[1] == '0') //The card is 10
                {
                    opponentPic = "_10_of_";
                }
                else if(opponentCard[1] == '1') //The card is jack
                {
                    opponentPic = "jack_of_";
                }
                else if(opponentCard[1] == '2') //The card is queen
                {
                    opponentPic = "queen_of_";
                }
                else //The card is king
                {
                    opponentPic = "king_of_";
                }
            }

            if((opponentCard[2] == 'd') || (opponentCard[2] == 'D')) //Diamonds
            {
                opponentPic = opponentPic + "diamonds";
            }
            else if((opponentCard[2] == 'h') || (opponentCard[2] == 'H')) //Hearts
            {
                opponentPic = opponentPic + "hearts";
            }
            else if((opponentCard[2] == 's') || (opponentCard[2] == 'S')) //Spades
            {
                opponentPic = opponentPic + "spades";
            }
            else //Clubs
            {
                opponentPic = opponentPic + "clubs";
            }
            
            Invoke((MethodInvoker)delegate { pictureBox1.Image = (Image)(Properties.Resources.ResourceManager.GetObject(opponentPic)); });

            int myCardValue = int.Parse(myCard.Substring(0, 2)); //Getting our card valur
            int opponentValue = int.Parse(opponentCard.Substring(0, 2)); //Getting our opponent card value

            if(myCardValue > opponentValue) //Checking who won the fight
            {
                myScore++;
                opponentScore--;
            }
            else if(myCardValue < opponentValue)
            {
                opponentScore++;
                myScore--;
            }

            //Changing the score shits
            Invoke((MethodInvoker)delegate { YourScore.Text = myScore.ToString(); });
            Invoke((MethodInvoker)delegate { OpponentScore.Text = opponentScore.ToString(); });

            //Nullifing our cards
            myCard = "0000";
            opponentCard = "0000";
        }

        /*
         * Function quits the game
         * Input: the sender and the argument
         * Output: none
         */
        private void Exit(object sender, EventArgs e)
        {
            MessageBox.Show("Your Score: " + YourScore.Text + "\nOpponent Score: " + OpponentScore.Text);
            Application.Exit();
        }
    }
}
